/* @flow */

"use strict"

import * as nsDB from './db.js';
import type {DB} from './db.js';

export function newDraw()/*:DRAW*/ { // match: cloneDraw().
    let canvas = document.getElementById( 'theCanvas' );
	// flow-ignore
    let context = canvas.getContext( '2d' );
    return {
        alpha: 1,
        canvas: canvas,
        context: context,
        ballFill: [0,0,0,1] // r, g, b, a.
    };
}

export function draw( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
    drawBackground( db, draw );
    drawForeground( db, draw );
}

function setRandomBackgroundFillStyle( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
    let mod = Math.floor( db.mNext.totalDelta ) % 3;
    switch( mod ) {
    case 0: draw.context.fillStyle = "rgba( 255, 64, 64, 0.2 )"; break;
    case 1: draw.context.fillStyle = "rgba( 64, 255, 64, 0.2 )"; break;
    case 2: draw.context.fillStyle = "rgba( 255, 64, 255, 0.2 )"; break;
    }
}

function cloneDraw( draw/*:DRAW*/ )/*:DRAW*/ {
    let draw2 = { // match: everything that sets fields in draw.
        alpha: draw.alpha,
        canvas: draw.canvas,
        context: draw.context,
        ballFill: draw.ballFill.slice()
    };
    return draw2;
}

function drawRandomRect1( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
	setRandomBackgroundFillStyle( db, draw );
    draw.context.fillRect( 0, 0, draw.canvas.width, draw.canvas.height );

}

function toFillStyle( r, g, b, a ) {
    return "rgba("+r+","+g+","+b+","+a+")";
}

function drawBackground( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
    draw.context.clearRect( 0, 0, draw.canvas.width, draw.canvas.height );
    drawRandomRect1( db, draw );
}

function drawBalls( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
    draw.context.fillStyle =
		toFillStyle( draw.ballFill[0],
                     draw.ballFill[1],
                     draw.ballFill[2],
                     draw.ballFill[3] * draw.alpha );
    nsDB.iterateBalls( db,
					   function( db/*:DB*/, ball ) {
						   draw.context.fillRect( ball.x, ball.y, ball.width, ball.height );
					   } );
}

function drawForeground( db/*:DB*/, draw/*:DRAW*/ )/*:void*/ {
    drawBalls( db, draw );
}

/*::
export type DRAW = {
alpha: number;
canvas: any,
context: any,
ballFill: Array<number> // r, g, b, a.
};
*/
