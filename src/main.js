/* @flow */

"use strict"

import * as nsDraw from './draw.js';
import * as nsDB from './db.js';
import * as nsEvents from './events.js';

export function mainjs() {
	let draw = nsDraw.newDraw();
	let db = nsDB.newDB( draw.canvas.width, draw.canvas.height );
	nsEvents.setupEvents( db );
	// flow-ignore
	MainLoop.setSimulationTimestep( 1000/db.k.FPS ); 
	MainLoop.setUpdate( function( delta ) {
		nsDB.beginFrame( db, delta );
		nsDB.handleEvents( db );
		nsDB.stepWorld( db );
	} );
	MainLoop.setDraw( function( interp ) {
		// todo: use interp.
		nsDraw.draw( db, draw );
	} );
	MainLoop.start();
}
