/* @flow */

"use strict"

import * as nsDB from './db.js';
import type {DB} from './db.js';

export function setupEvents( db/*:DB*/ )/*:void*/ {
	let k = db.k;

	let onClick = function(e) { nsDB.addEvent( db, k.ON_CLICK_EVENT ); };
	// flow-ignore
    $('#theCanvas').click( onClick );

	let pauseKey = function(e,h) { nsDB.addEvent( db, k.ON_PAUSE_KEY_EVENT ); };
	// flow-ignore
    hotkeys( 'p', pauseKey );

	let increaseKey = function(e,h) { nsDB.addEvent( db, k.ON_INCREASE_KEY_EVENT ); };
	hotkeys( '=', increaseKey );
    hotkeys( '+', increaseKey );

	let decreaseKey = function(e,h) { nsDB.addEvent( db, k.ON_DECREASE_KEY_EVENT ); };
    hotkeys( '-', decreaseKey );
    hotkeys( '_', decreaseKey );
}


