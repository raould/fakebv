/* @flow */

"use strict"

import * as Random from './random.js';

export function newDB( width/*:number*/, height/*:number*/ )/*:DB*/ { // match: cloneDB().
    let db = {};

	// (implicitly) immutables.
    db.k = {
        FPS: 30,
        MAX_BALLS: 10,
        BALL_WIDTH: 10,
        BALL_HEIGHT: 10,
        FUTURE_ITERATIONS: 100,
        ON_CLICK_EVENT: "onClickEvent",
        ON_PAUSE_KEY_EVENT: "onPauseKeyEvent",
        ON_INCREASE_KEY_EVENT: "onIncreseKeyEvent",
        ON_DECREASE_KEY_EVENT: "onDecreaseKeyEvent",
    };

	// mutables; swapped on every update.
	let mfn = function( k/*:CONSTANTS*/ )/*:MUTABLES*/ {
		return {
			events: [],
			paused: false,
			frameDelta: 0,
			totalDelta: 0,
			futureIterations: 100,
			wallCounter: 0,
			maxBalls: k.MAX_BALLS,
			world: makeWorld( width, height, db.k ),
			agitation: 1,
			agitationCenter: 1,
			agitationDeltaLimit: 1,
			agitationStep: 0.1,
		}
	};
	db.mNow = mfn( db.k );
	db.mNext = mfn( db.k );

    return db;
}

function makeWorld( width/*:number*/, height/*:number*/, k/*:CONSTANTS*/ )/*:WORLD*/ {
	let w/*:WORLD*/ = {
		width: width,
		height: height,
		balls: defaultBalls( width, height, k ),
	};
	return w;
}

function defaultBalls( width/*:number*/, height/*:number*/, k/*:CONSTANTS*/ )/*:Array<BALL>*/ { // match: cloneBalls().
	let balls = [];
    for( let i = 0; i < k.MAX_BALLS; ++i ) {
		let b/*:BALL*/ = {
            x: Random.random01()*width-k.BALL_WIDTH,
            y: Random.random01()*height-k.BALL_HEIGHT,
            width: k.BALL_WIDTH,
            height: k.BALL_HEIGHT,
            vx: (0.01 + Random.random01()*0.02) * Random.randomSign(),
            vy: (0.01 + Random.random01()*0.02) * Random.randomSign(),
            generation: 0
        };
		balls[i] = b;
    }
	return balls;
}

export function beginFrame( db/*:DB*/, delta/*:number*/ )/*:void*/ {
	let m = db.mNext;
	shiftMutables( db );
	m.totalDelta += m.frameDelta;
}

function shiftMutables( db/*:DB*/ )/*:void*/ {
	let mOrig = db.mNow;
	db.mNow = db.mNext;
	copyMutables( mOrig, db.mNext );
}

function copyMutables( src/*:MUTABLES*/, dst/*:MUTABLES*/ )/*:void*/ {
	dst.events = src.events;
	dst.paused = src.paused;
	dst.frameDelta = src.frameDelta;
    dst.totalDelta = src.totalDelta;
    dst.futureIterations = src.futureIterations;
    dst.wallCounter = src.wallCounter;
    dst.maxBalls = src.maxBalls;
	copyWorld( src.world, dst.world );
    dst.agitation = src.agitation;
    dst.agitationCenter = src.agitationCenter;
    dst.agitationDeltaLimit = src.agitationDeltaLimit;
    dst.agitationStep = src.agitationStep;
}

function copyWorld( src/*:WORLD*/, dst/*:WORLD*/ )/*:void*/ {
    dst.width = src.width;
    dst.height = src.height;
    dst.balls = [];
	copyBalls( src.balls, dst.balls );
}

function copyBalls( src/*:Array<BALL>*/, dst/*:Array<BALL>*/ )/*:void*/ { // match: generateBalls().
	let len = Math.min( src.length, dst.length );
	for( let i = 0; i < len; ++i ) {
        dst[i].x = src[i].x,
        dst[i].y = src[i].y,
        dst[i].width = src[i].width,
        dst[i].height = src[i].height,
        dst[i].vx = src[i].vx,
        dst[i].vy = src[i].vy,
        dst[i].generation = src[i].generation
    };
}

export function addEvent( db/*:DB*/, event/*:EVENTNAME*/ )/*:void*/ {
	db.mNext.events.push( event );
}

export function iterateBalls( db/*:DB*/, fn/*:(db:DB,b:BALL)=>void*/ )/*:void*/ {
    let w = db.mNext.world;
    for( let i = 0; i < w.balls.length; ++i ) {
        fn( db, w.balls[i] );
    }
}

export function collideBallWithWalls( db/*:DB*/, ball/*:BALL*/ )/*:void*/ {
    let w = db.mNext.world;
    if( ball.x > w.width-ball.width ) {
        ball.x = w.width-ball.width;
        ball.vx = -Math.abs(ball.vx);
    }
    if( ball.x < 0 ) {
        ball.x = 0;
        ball.vx = Math.abs(ball.vx);
    }
    if( ball.y > w.height-ball.height ) {
        ball.y = w.height-ball.height;
        ball.vy = -Math.abs(ball.vy);
    }
    if( ball.y < 0 ) {
        ball.y = 0;
        ball.vy = Math.abs(ball.vy);
    }
}

export function limitAgitation( db/*:DB*/ )/*:void*/ {
    let m = db.mNext;
    m.agitation = Math.min( m.agitation, m.agitationCenter + m.agitationDeltaLimit );
    m.agitation = Math.max( m.agitation, m.agitationCenter - m.agitationDeltaLimit );
}

export function increaseAgitation( db/*:DB*/ )/*:void*/ {
    let m = db.mNext;
    m.agitation += m.agitationStep;
    limitAgitation( db );
}

export function decreaseAgitation( db/*:DB*/ )/*:void*/ {
    let m = db.mNext;
    m.agitation -= m.agitationStep;
    limitAgitation( db );
}

export function getRandomAgitation( db/*:DB*/ )/*:number*/ {
	let m = db.mNext;
    return m.agitation * Random.random01( db );
}

export function updateBalls( db/*:DB*/ )/*:void*/ {
    iterateBalls( db,
                  function( db/*:DB*/, ball/*:BALL*/ ) {
					  let m = db.mNext;
                      ball.x += ball.vx * m.frameDelta * getRandomAgitation( db );
                      ball.y += ball.vy * m.frameDelta * getRandomAgitation( db );
                      collideBallWithWalls( db, ball );
                  } );
}

export function handleEvents( db/*:DB*/ )/*:void*/ {
    let k = db.k;
	let m = db.mNext;
    if( m.events.length > 0 ) {
        console.log( m.events.length );
        for( let i = 0; i < m.events.length; ++i ) {
            let e = m.events[i];
            console.log( e );
            switch( e ) {
            case k.ON_CLICK_EVENT:
                m.paused = ! m.paused;
                console.log( m.paused );
                break;
            case k.ON_PAUSE_KEY_EVENT:
                m.paused = ! m.paused;
                console.log( m.paused );
                break;
            case k.ON_INCREASE_KEY_EVENT:
                increaseAgitation( db );
                console.log( m.agitation );
                break;
            case k.ON_DECREASE_KEY_EVENT:
                decreaseAgitation( db );
                console.log( m.agitation );
                break;
            default:
				throw Error( "error: unsupported event " + e );
            }
        }
        m.events = [];
    }
}

export function stepWorld( db/*:DB*/ )/*:void*/ {
	let m = db.mNext;
    if( false == m.paused ) {
		updateBalls( db );
		// todo: update other things.
    }
    m.wallCounter++;
}

/*::
export type EVENTNAME = string;

export type CONSTANTS = {
	FPS: number;
	MAX_BALLS: number;
	BALL_WIDTH: number;
	BALL_HEIGHT: number;
	FUTURE_ITERATIONS: number;
	ON_CLICK_EVENT: EVENTNAME;
	ON_PAUSE_KEY_EVENT: EVENTNAME;
	ON_INCREASE_KEY_EVENT: EVENTNAME;
	ON_DECREASE_KEY_EVENT: EVENTNAME;
};

export type MUTABLES = {
	events: Array<EVENTNAME>;
	paused: boolean;
	frameDelta: number;
	totalDelta: number;
	futureIterations: number;
	wallCounter: number;
	maxBalls: number;
	world: WORLD;
	// agitation stuff is complicated hacky junk.
	// at least 'agitation' should be int >= 0.
	agitation: number;
	agitationCenter: number;
	agitationDeltaLimit: number;
	agitationStep: number;
};

export type BALL = {
	x: number;
	y: number;
	width: number;
	height: number;
	vx: number;
	vy: number;
	generation: number;
};

export type WORLD = {
	width: number;
	height: number;
	balls: Array<BALL>;
};

export type DB = {
	k: CONSTANTS;
	mNow: MUTABLES;
	mNext: MUTABLES;
};
*/
