#!/bin/bash

set -xe

SRC_DIR=src
DEFLOWED_DIR=deflowed
MERGED_DIR=merged
BUNDLEJS_FILE=bundle.js

BABEL_EXE=babel
BABEL_PLUGINS='--plugins transform-flow-strip-types,transform-es2015-modules-commonjs'
BROWSERIFY_EXE=browserify
PRETTYJS_EXE=pretty-js
FLOW_EXE=flow

function ensureCleanDir {
	if ! [ -e ${1} ]; then mkdir ${1}; fi
	\rm -rf ${1}/*
}

cd ${SRC_DIR}
flow check --json | ${PRETTYJS_EXE}
cd ..

ensureCleanDir ${DEFLOWED_DIR}
cd ${SRC_DIR}
${BABEL_EXE} ${BABEL_PLUGINS} --out-dir=../${DEFLOWED_DIR} *.js
cd ..

ensureCleanDir ${MERGED_DIR}
cd ${DEFLOWED_DIR}
${BROWSERIFY_EXE} -r ./main.js:main main.js | ${PRETTYJS_EXE} > ../${MERGED_DIR}/${BUNDLEJS_FILE}
cd ..

