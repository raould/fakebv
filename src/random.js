/* @flow */

"use strict"

/*
todo: something like this

uint64_t s[2];

static inline uint64_t rotl(const uint64_t x, int k) {
return (x << k) | (x >> (64 - k));
}

uint64_t next(void) {
const uint64_t s0 = s[0];
uint64_t s1 = s[1];
const uint64_t result = s0 + s1;

s1 ^= s0;
s[0] = rotl(s0, 55) ^ s1 ^ (s1 << 14); // a, b
s[1] = rotl(s1, 36); // c

return result;
}
*/

export function random01()/*:number*/ {
    return Math.random();
}

export function randomSign()/*:number*/ {
    if( Math.random() < 0.5 ) { 
        return -1;
    }
    else {
        return 1;
    }
}
